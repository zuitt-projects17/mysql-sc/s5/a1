# WDC040-S05: MySQL Classic Models

Before executing the SQL code from <code>classic-models-export.sql</code>, uncheck the **Enable foreign key checks** option located below the SQL editor pane.

![](images/disable-foreign-key-checks.png)